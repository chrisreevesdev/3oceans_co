<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', '3oceans_co');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '@O>TYu7#$L{@%b%?n0.1Is8i*Ug$]1DC.=Pd3LRUbj8gL36Jh[tJ|jy@MOF]c^+F');
define('SECURE_AUTH_KEY',  't&GoVNX=~{#P2kmN1jvb:+Ltj-UViH!JoFb lQ+G-gr*U,ze2beI2]G$dIMkcgy2');
define('LOGGED_IN_KEY',    '/~ngM3ZzIxyZh~%`M-;^dE4?J9h4l*^70IRg_iT:G_MjI >M936]sw+Ywm}Op%X[');
define('NONCE_KEY',        '^=|>g1gA$$K<V 76(.R8+_$I7+wtfgfGEIZwVrrAF/i*EvQQNl2 `tYVF{uEnQ0q');
define('AUTH_SALT',        'VP;A D?dCBq(Fg)sFzh.f4zvTZi5Y2xjlS=v>%-,<z2}h&L6fV*:v$RY[Ugpcq^q');
define('SECURE_AUTH_SALT', 'NPqs%mQGJREadw1~PQ5DF}3iM:*o3=!{Lc4`4<>x3t_Kbr&<a@WHtd[cD5K[NF_0');
define('LOGGED_IN_SALT',   'HI`>}V8^o`1PV?V/`lexOt.Pb+cNM0X`CS7TDMiM9d/yB2 |YMvBSg]S-2q)# >q');
define('NONCE_SALT',       'U[Tc>C+X)ti@p4qx)h_%~cnfi@z!g9yV3J)(dUGZ_orNUoGnG(0T5E;E{Le/qdss');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
