<?php get_header(); ?>


	<?php /* Start - Gallery */ ?>
	<?php 
		$show_gallery = get_field('show_showcase_gallery');
		$gallery_images = get_field('showcase_image_gallery');
	?>
	<?php if ( $show_gallery ) { ?>
		<div class="section overflow-y-show">
			<div class="gallery grid grid--collapsed grid--align-center">
				<div class="cell cell--align-center">
					<?php masterslider(2); ?> 
				</div>
			</div>
		</div>
	<?php } ?>
	<?php /* End - Gallery */ ?>


	

	<?php /* Start - About boxes banner */ ?>
	<?php 
		$show_box_1 = get_field('show_box_1');
		$show_box_2 = get_field('show_box_2');
	?>

	<?php if ( $show_box_1 || $show_box_1 ) { ?>
		<div class="section fp-auto-height">
	<?php } ?>

	<?php 
		$about_box_1_title = get_field('about_box_1_title'); 
		$about_box_1_text = get_field('about_box_1_text');
		$about_box_1_img = get_field('about_box_1_image');
	?>
	<?php /* Start - About box 1 */ ?>
	<?php if ( $show_box_1 ) { ?>
			<div class="about_box_1 grid grid--align-center">
				<div class="cell cell--1/2 text_box">
					<div class="box_text">
						<h2><?php echo $about_box_1_title; ?></h2>
						<?php echo $about_box_1_text; ?>
					</div>
				</div>
				<div class="cell cell--1/2 img_box">
					<div class="box_img">
						<div class="animation"></div>
						<img src="<?php echo $about_box_1_img; ?>" />
					</div>
				</div>
			</div>
	<?php } ?>
	<?php /* End - About box 1 */ ?>

	<?php /* Start - About box 2 */ ?>
	<?php 
		$about_box_2_title = get_field('about_box_2_title'); 
		$about_box_2_text = get_field('about_box_2_text');
		$about_box_2_img = get_field('about_box_2_image');
	?>
	<?php if ( $show_box_2 ) { ?>
		<div class="about_box_2 grid grid--align-center">
			<div class="cell cell--1/2 img_box">
				<div class="box_img">
					<div class="animation"></div>
					<img src="<?php echo $about_box_2_img; ?>" />
				</div>
			</div>
			<div class="cell cell--1/2 text_box">
				<div class="box_text">
					<h2><?php echo $about_box_2_title; ?></h2>
					<?php echo $about_box_2_text; ?>
				</div>
			</div>
		</div>
	<?php } ?>
	<?php /* End - About box 2 */ ?>

	<?php if ( $show_box_1 || $show_box_1 ) { ?>
		</div>
	<?php } ?>
	<?php /* End - About boxes banner */ ?>




	<?php /* Start - Projects */ ?>
	<?php 
		$show_projects = get_field('show_projects');
		$projects_title = get_field('project_heading_title');
		$projects_images = get_field('project_images');
	?>
	<?php if ( $show_projects ) { ?>
		<div class="section fp-auto-height">
			<div class="projects grid grid--align-center">
				<div class="cell">
					<div class="container">
						<div class="grid">
							<div class="cell">
								<h2><?php echo $projects_title; ?></h2>
							</div>
						</div>
					</div>

					<div class="grid">
						<div class="cell">
							<img src="<?php echo $projects_images[0]['url']; ?>" />
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php } ?>
	<?php /* End - Projects */ ?>


	<?php /* Start - Contact */ ?>
	<?php 
		$show_contact = get_field('show_contact_map');
		$map_background = get_field('map_background');
		$mobile_map_background = get_field('mobile_map_background');
		$map_background = get_field('map_background'); 
		$contact_details = get_field('contact_text'); 
		$contact_directions_link = get_field('direction_link'); 
		$contact_note_link = get_field('note_link'); 
	?>
	<?php if ( $show_contact ) { ?>
		<div class="section fp-auto-height">
			<div class="contact_wrapper grid grid--align-strech">
				<div class="content_bg cell cell-align-stretch">

					<style>
						.contact_wrapper .content_bg .map{ background-image: url(<?php echo $map_background;?>); }
						@media screen and (max-width: 480px) {
							.contact_wrapper .content_bg .map{ background-image: url(<?php echo $mobile_map_background;?>); }
						}
					</style>
					<div class="map">
					</div>

					<div class="map_overlay">
						<div class="contact_text_wrapper">
							<?php echo $contact_details; ?>
							<?php if($contact_directions_link){ ?>
								<a class="directions" target="_blank" href="<?php echo $contact_directions_link; ?>">Get Directions</a>
							<?php } ?>
							<?php if($contact_note_link){ ?>
								<br>
								<a class="note" href="#">Leave a note</a>
							<?php } ?>
						</div>
					</div>
				</div>

				<div class="contact_form_popup" id="contact_form_popup">
					<div class="contact_form_inner">
						<div class="close">
							<span class="top"></span>
							<span class="bottom"></span>
						</div>
						<?php gravity_form( 2, true, false, false, '', true ); ?>
					</div>
				</div>

			</div>
		</div>
	<?php } ?>
	<?php /* End - Projects */ ?>


	<?php /* Start - Bottom banner */ ?>
	<?php 
		$show_banner = get_field('show_bottom_banner');
		$banner_video_id = get_field('bottom_banner_video_id');
		$banner_video_preload_image = get_field('bottom_banner_video_preload_image');
		$banner_video_height = get_field('bottom_banner_video_height');
		$banner_video_width = get_field('bottom_banner_video_width');
		$banner_img = get_field('bottom_banner_image'); 
		$banner_text = get_field('bottom_banner_title');
		$banner_button_text = get_field('bottom_banner_button_text'); 
		$banner_button_link = get_field('bottom_banner_button_link'); 

		$bottom_banner_video = $video_url = "//player.vimeo.com/video/".$banner_video_id."?background=1&autopause=0";
	?>
	<?php if ( $show_banner ) { ?>
		<div class="section bottom_banner_wrapper">
			<div class="bottom_banner grid grid--collapsed grid--align-stretch" style="background-image: url('<?php echo $banner_img; ?>');">
				<div class="cell cell--align-stretch">

					<div class="banner_text_wrapper full_page_height">

						<div class="video parallax-bottom-banner">

						    <iframe class="vimeo" src="<?php echo $bottom_banner_video; ?>" data-height="<?php echo $banner_video_height; ?>" data-width="<?php echo $banner_video_width; ?>" allowfullscreen></iframe> 

						    <div class="preload_image" style="background-image: url('<?php echo $banner_video_preload_image; ?>');"> 
							    <?php //masterslider(2); ?> 
							</div>

							<div class="video_overlay fp-bg cell--align-center">
								<div class="video_overlay_inner">
									<h3><?php echo $banner_text; ?></h3>

									<a href="<?php echo $banner_button_link; ?>"><?php echo $banner_button_text; ?></a>
								</div>
								<div class="section3_line_right"></div>
							</div>

						</div>

					</div>
				</div>
			</div>
		</div>
	<?php } ?>

	<?php /* End - Bottom banner */ ?>



	<?php /* Start - Main content section */ ?>
	<?php if ( !($show_gallery || $show_box_1 || $show_box_2 || $show_projects || $show_contact || $show_banner) ) { ?>
		
		<div class="section">

			<h1><?php the_title(); ?></h1>

		<?php if (have_posts()): while (have_posts()) : the_post(); ?>

			<?php /* Start - article */ ?>
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<?php the_content(); ?>

				<?php comments_template( '', true ); // Remove if you don't want comments ?>

				<br class="clear">

				<?php edit_post_link(); ?>

			</article>
			<?php /* End - article */ ?>

		<?php endwhile; ?>

		<?php else: ?>

			<?php /* Start - article */ ?>
			<article>

				<h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>

			</article>
			<?php /* End - article */ ?>

		<?php endif; ?>

		</div>
		<?php /* End - Main content section */ ?>

	<?php } ?>


<?php //get_sidebar(); ?>

<?php get_footer(); ?>
