<?php get_header(); ?>

	<!-- section -->
	<div class="section fp-auto-height container">

		<div class="grid single_page">

			<div class="cell cell--3/4 post">

				<?php if (have_posts()): while (have_posts()) : the_post(); ?>

					<a class="back_to" href="<?php echo get_permalink( get_option( 'page_for_posts' ) ); ?>">Back to <?php echo get_the_title(get_option( 'page_for_posts' )); ?>
					</a>

					<!-- article -->
					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

						<!-- post title -->
						<h1><?php the_title(); ?></h1>
						<!-- /post title -->

						<!-- post details -->
						<div class="post_data">
							<span class="date"><?php the_time('F j, Y'); ?> <?php the_time('g:i a'); ?></span>
							<span class="author"><?php echo get_the_author(); ?></span>
						</div>
						<!-- /post details -->

						<?php the_content(); // Dynamic Content ?>

					</article>
					<!-- /article -->

				<?php endwhile; ?>

				<?php else: ?>

					<!-- article -->
					<article>

						<h1><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h1>

					</article>
					<!-- /article -->

				<?php endif; ?>

			</div>


			<div class="cell cell--1/4 post_sidebar">

				<?php get_sidebar(); ?>

			</div>

		</div>

	</div>
	<!-- /section -->




<?php get_footer(); ?>
