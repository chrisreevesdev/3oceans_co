<?php
$i = 1;
$post_count = $GLOBALS['wp_query']->post_count;
?>

<?php if (have_posts()): while (have_posts()) : the_post(); ?>

	<?php if ($i % 2 == 1) { ?>
		<div class="grid news">
	<?php } ?>
	
	<!-- article -->
	<div class="cell cell--1/2">
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

			<!-- post thumbnail -->
			<?php 
			if ( has_post_thumbnail()) {
				$thumbnail = get_the_post_thumbnail_url();
			} else {
				$thumbnail = get_template_directory_uri()."/img/blank_article_img.jpg";
			}

			$categories = get_the_category();
			?>
			<!-- /post thumbnail -->

			<div class="article_wrapper" style="background-image: url('<?php echo $thumbnail; ?>');">
			
				<div class="article_data">
					<div class="info">
						<span class="date"><?php the_time('j F Y'); ?></span>
						<span class="category"><?php echo $categories[0]->name ?></span>
					</div>
					<!-- post title -->
					<h2>
						<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
					</h2>
					<!-- /post title -->
				</div>

			</div>

			<?php //html5wp_excerpt('html5wp_index'); // Build your custom callback length in functions.php ?>

			<?php //edit_post_link(); ?>

		</article>
	</div>
	<!-- /article -->

	<?php if (($i % 2 == 0)) { ?>
		</div>
	<?php } ?>

	<?php $i++; ?>

<?php endwhile; ?>


<?php if (($i-1)%2 == 1) { //Div end for non complete cells ?>
	</div>
<?php } ?>


<?php else: ?>

	<div class="cell">
		<!-- article -->
		<article>
			<h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>
		</article>
		<!-- /article -->
	</div>

<?php endif; ?>