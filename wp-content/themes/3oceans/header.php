<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>

		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>

		<link href="//www.google-analytics.com" rel="dns-prefetch">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon.ico" rel="shortcut icon">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/touch.png" rel="apple-touch-icon-precomposed">

		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="<?php bloginfo('description'); ?>">

		<?php wp_head(); ?>
		<script>
        // conditionizr.com
        // configure environment tests
        conditionizr.config({
            assets: '<?php echo get_template_directory_uri(); ?>',
            tests: {}
        });
        </script>

	</head>
	<body <?php body_class(); ?>>

		<?php /* Start - fullscreen */ ?>
		<?php if (is_front_page()){ ?>
		<div id="fullscreen">
			<a class="open">Full Screen<span class="outer"><span class="inner"></span></span></a>
		</div>
		<?php } ?>
		<?php /* End - fullscreen */ ?>


		<?php /* Start - perloader */ ?>
		<div id="preloader"></div>
		<?php /* End - perloader */ ?>


		<?php /* Start - wrapper */ ?>
		<div class="container-fluid">


			<?php

				$logo = get_field('custom_logo');
				if (is_null($logo) || !$logo){
					if (get_option('logo') != ''){
						$logo = get_option('logo');	
					} else {
						$logo = get_template_directory_uri()."/img/3_Oceans.gif";
					}
				}
			?>

			<?php /* Start - header */ ?>
			<header class="header grid grid--collapsed">
				
				<div class="cell">

					<div class="grid grid--collapsed menu_wrapper">
						<div class="cell">
							<?php /* Start - logo */ ?>
							<div class="logo">
								<a href="<?php echo home_url(); ?>">
									<?php if (is_front_page()){ ?>
										<img src="<?php echo $logo; ?>" alt="Logo" class="logo-img">
									<?php } else { ?>
										<img src="<?php echo get_template_directory_uri(); ?>/img/blank.png" alt="Logo" data-src="<?php echo $logo; ?>" class="logo-img">
									<?php } ?>
								</a>
							</div>
							<?php /* End - logo */ ?>

							<?php /* Start - nav */ ?>
							<div class="menue_container">

								<div class="button_container" id="toggle">
									<span class="top"></span>
									<span class="middle"></span>
									<span class="bottom"></span>
								</div>

								<div class="overlay" id="overlay">
									<nav class="overlay-menu">
										<?php html5blank_nav(); ?>
									</nav>
								</div>

							</div>
							<?php /* End - nav */ ?>
						</div>
					</div>

					

				</div>

			</header>

			<?php /* Start - Fullpage */ ?>
			<div id="fullpage">

			<?php 

				$show_header_video = get_field('show_header_video');
				$header_video_id = get_field('header_video_id');
				$header_video_preload_image = get_field('header_preload_image');
				$header_video_height = get_field('header_video_height');
				$header_video_width = get_field('header_video_width');
				$header_video_titles = get_field('header_video_title');
				$header_video_button_text = get_field('header_video_button_text');
				$header_video_button_link = get_field('header_video_button_link');
				$show_bottom_cover = get_field('show_bottom_cover');


				if (is_blog()){
					$show_header_video = true;
					$header_video_id = get_option('header_video');
					$header_video_height = get_option('header_video_height');
					$header_video_width = get_option('header_video_width');
					$header_video_preload_image = get_option('header_image');
					$show_bottom_cover = true;
				}

				$video_url = "//player.vimeo.com/video/".$header_video_id."?background=1";


				if($header_video_height > 0 && $header_video_width > 0 ) {
					$video_height = (($header_video_height / $header_video_width) * 100).'vw';
					$video_width = (($header_video_width / $header_video_height) * 100).'vh';
				}

				$header_video_class = "";
				$section_video_class = "fp-auto-height";
				if (is_front_page()){
					$header_video_class = "full_page_height";
					$section_video_class = "";
				}
			?>
			<?php if ($show_header_video) { ?>
				<div class="section <?php echo $section_video_class; ?> main_slider_wrapper">
					<div class="grid grid--collapsed video_wrapper <?php echo $header_video_class; ?> grid--align-stretch grid--align-center">
						<div class="cell cell--align-stretch">
							<div class="video parallax-main-slider">
							    
							    <iframe class="vimeo" src="<?php echo $video_url; ?>" data-height="<?php echo $header_video_height; ?>" data-width="<?php echo $header_video_width; ?>" allowfullscreen></iframe>

							    <div class="preload_image" style="background-image: url('<?php echo $header_video_preload_image; ?>');">  
							    	<?php //masterslider(2); ?> 
								</div>

								<?php if ($header_video_titles && $header_video_button_text) { ?>
								<div class="video_overlay fp-bg">
									<div class="video_overlay_inner">
										<h1 class="tlt">
											<ul class="texts">
												<?php 
												$title_texts = explode("|", $header_video_titles);
												foreach ($title_texts as $title_text) { ?>
													<li data-in-effect="fadeIn" data-out-effect="fadeOut"><?php echo trim($title_text) ?></li>
												<?php } ?>
											</ul>
										</h1>


										<a href="<?php echo $header_video_button_link; ?>"><?php echo $header_video_button_text; ?></a>
										<br>
										<a href="#" class="main_slider_mobile_vid">View More</a>
										<div class="section1_line"></div>
									</div>
								</div>
								<?php } ?>

							</div>
						</div>
					</div>

					<?php if($show_bottom_cover) { ?>
						<div class="white_bottom_cover grid grid--collapsed grid--align-stretch">
							<div class="cell">
								<div class="white_cover">
									<div class="white_bg"></div>
								</div>
							</div>
						</div>
					<?php } ?>
				</div>
			<?php } ?>
			<?php /* End - header */ ?>
