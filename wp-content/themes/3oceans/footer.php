			<?php /* Start - footer */ ?>
			<div class="section fp-auto-height">
				<footer class="footer grid grid--align-center">
					<div class="cell">
						<?php /* Start - footer menu */ ?>
						<nav class="nav">
							<?php html5footer_nav(); ?>
						</nav>
						<?php /* End - footer menu */ ?>
					</div>
				</footer>
			</div>
			<?php /* End - footer */ ?>

		</div>
		<?php /* End - wrapper */ ?>


		</div>
		<?php /* End - Fullpage */ ?>


		<?php wp_footer(); ?>

		<?php /* End - analytics */ ?>
		<?php /* <script>
		(function(f,i,r,e,s,h,l){i['GoogleAnalyticsObject']=s;f[s]=f[s]||function(){
		(f[s].q=f[s].q||[]).push(arguments)},f[s].l=1*new Date();h=i.createElement(r),
		l=i.getElementsByTagName(r)[0];h.async=1;h.src=e;l.parentNode.insertBefore(h,l)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		ga('create', '‘UA-101985722-1', 'yourdomain.com');
		ga('send', 'pageview');
		</script>
		*/ ?>

	</body>
</html>
