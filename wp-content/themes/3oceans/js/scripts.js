(function ($, root, undefined) {
	
	$(function () {


		$(document).ready(function() {

			$('#preloader').fadeOut('slow',function(){
				
				$(this).remove();

				//Delay logo load
				setTimeout(function(){ 
					logo_src = $('.header .logo img').data( "src" );
					$('.header .logo img').attr("src",logo_src);
				}, 500);
			});

			//Fullpage.js int
			$('body.home #fullpage').fullpage({
				lazyLoading: false,
				scrollingSpeed: 1400,
				minDisplayTime: 2000,
				parallax:true,
				parallaxKey: 'M29jZWFucy1jby5jcmN0ZXN0aW5nLmNvbV94cjJjR0Z5WVd4c1lYZz1XOGY=',
				scrollBar: true,
				scrollHorizontally: false,
			});
			video_resize();

			//Social share init
			SocialShareKit.init();

			//Text animation
			$(function () {
				$('.tlt').textillate({
					initialDelay: 1000,
					loop: true,
					minDisplayTime: 3000
				});
			});


			$(window).scroll(function() {
				main_slider = $('body:not(.home) .parallax-main-slider');
				//console.log($(document).scrollTop());
				if ( $(document).scrollTop() > 0 && $(document).scrollTop() < 400){
					main_slider.offset({ top: $(document).scrollTop()*0.7 })
				} else {
					main_slider.offset({ top: 5 })
				}
				// if($(document).scrollTop() > 100 && $(document).scrollTop() < 400) {
				// 	$('body:not(.home) .main_slider_wrapper .parallax-main-slider').addClass('active');
				// }
				// else {
				// 	$('body:not(.home) .main_slider_wrapper .parallax-main-slider').removeClass('active');
				// }
			});
			// $(window).scrollTop(0);
			// //Parallax - main slider
			// if($('.parallax-main-slider').length){
			// 	var parallaxTop1 = $('.parallax-main-slider').offset().top;
			// 	var parallaxStart1 = parallaxTop1 * 0.9;
			// }

			// //Parallax - bottom banner
			// if($('.parallax-bottom-banner').length){
			// 	var parallaxTop2 = $('.bottom_banner').offset().top;
			// 	var parallaxStart2 = parallaxTop2 * 0.9;
			// }

			// $(window).scroll(function() {
			// 	//Parallax - mainslider
			// 	if ($('.parallax-main-slider').length && ($(this).scrollTop() >= parallaxStart1)) {
			// 		$('.parallax-main-slider').css({
			// 			'bottom': -(($(this).scrollTop() - parallaxStart1) / 2) + "px"
			// 		});
			// 	}

			// 	//Parallax - bottom banner
			// 	if ($('.parallax-bottom-banner').length && ($(this).scrollTop() >= parallaxStart2)) {
			// 		$('.parallax-bottom-banner').css({
			// 			'bottom': -((($(this).scrollTop() - parallaxStart2) / 2)-112) + "px"
			// 		});
			// 	}
			// });

		});


		if (!is_mobile()){
			$('iframe.vimeo').each(function( index ) {
				var main_frame = $(this);
				var player = new Vimeo.Player(main_frame);
				player.play();

			    player.on('timeupdate', function(time) {
			    	if (time.seconds > 0.01 && time.seconds < 2){
				        // console.log(time.seconds);
				        main_frame.next().fadeOut();
				    }
			    });
			});
		}



		$( window ).resize(function() {
			//Video area resize
			video_resize();

			$('.full_page_height').each(function() {
			    $(this).height($(window).height());
			});
		});


		function video_resize(){
			$('.video iframe').each(function( index ) {
				$vid_h = $(this).attr("data-height");
				$vid_w = $(this).attr("data-width");

				$outer_h = $(this).parent().height();
				$outer_w = $(this).parent().width();

				$vid_n_h = ($vid_h/$vid_w)*$outer_w; //Calculate new width based on height
				$vid_n_w = ($vid_w/$vid_h)*$outer_h; //Calculate new height based on width

				if ($vid_n_h>$outer_h){
					$(this).width($outer_w);
					$(this).height($vid_n_h);
					$(this).css({"left":-0, "top":-($vid_n_h-$outer_h)/2});
				} else if($vid_n_w>$outer_w){
					$(this).height($outer_h);
					$(this).width($vid_n_w);
					$(this).css({"top":0, "left":-($vid_n_w-$outer_w)/2});
				}

			});
		}

		$('.full_page_height').each(function() {
		    $(this).height($(window).height());
		});

		// $('.main_slider_wrapper .video_wrapper').height($(window).height());
		// $('.video_wrapper .video iframe').height($(window).height());

		$('#toggle').click(function() {
			$(this).toggleClass('active');
			$('#overlay').toggleClass('open');
		});
		


		// Full screen
		FULL_SCREEN = false;
		$('#fullscreen .open').click(function() {
            
            // if(!FULL_SCREEN){
            // 	$('.body').fullscreen();
            // 	setTimeout(video_resize, 100);
            // 	FULL_SCREEN = true;
            // }
            // else{
            // 	$.fullscreen.exit();
            // 	setTimeout(video_resize, 100);
            // 	FULL_SCREEN = false;
            // }
            $('body').fullscreen();
        	setTimeout(video_resize, 100);
            return false;
        });




		// Popup box
		popup_toggle("#contact_form_popup");
		$("body.contact .contact_wrapper a.note").click(function(event) {
			event.preventDefault();
			popup_toggle("#contact_form_popup");
		});

		//Button click event
		$("#contact_form_popup .close").click(function(event) {
			event.preventDefault();
			popup_toggle("#contact_form_popup");
		});

		function popup_toggle(popup){
			if ($(popup).hasClass("active")){
				$(popup).removeClass("active");
			} else {
				$(popup).addClass("active");
			}
		}




		


		function is_mobile(){
			var isMobile = false; //initiate as false
			if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) 
	    || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) isMobile = true;

			return isMobile;
		}




	});
	
})(jQuery, this);


