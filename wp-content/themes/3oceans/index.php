<?php get_header(); ?>

	<!-- section -->
	<div class="section fp-auto-height">

		<div class="container blog_wrapper">

			<!-- Blog page header -->
			<div class="grid">
				<div class="cell">
					<h1><?php _e( 'Latest News', 'html5blank' ); ?></h1>
					<div class="title_separator"></div>
				</div>
			</div>
			<!-- /Blog page header -->


			<!-- Blog article loop -->
			<?php get_template_part('loop'); ?>
			<!-- /Blog article loop -->


			<!-- Pegination -->
			<div class="grid">
				<div class="cell">
					<?php get_template_part('pagination'); ?>
				</div>
			</div>
			<!-- /Pegination -->


		</div>


	</div>
	<!-- /section -->


<?php get_footer(); ?>
